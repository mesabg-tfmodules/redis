terraform {
  required_version = ">= 1.0.0"

  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = ">= 4.0.0"
    }
  }
}

resource "aws_elasticache_subnet_group" "subnet_group" {
  name       = "${var.name}-${var.environment}"
  subnet_ids = var.subnet_ids
}

resource "aws_elasticache_cluster" "redis" {
  cluster_id                = var.environment
  engine                    = "redis"
  engine_version            = "6.x"
  parameter_group_name      = "default.redis6.x"
  node_type                 = var.node_type
  num_cache_nodes           = var.num_cache_nodes
  port                      = var.port
  apply_immediately         = true
  subnet_group_name         = aws_elasticache_subnet_group.subnet_group.name
  security_group_ids        = var.security_group_ids
  snapshot_window           = var.snapshot_window
  snapshot_retention_limit  = var.snapshot_retention_days

  tags = {
    Name            = "${var.name} - ${var.environment}"
    Environment     = var.environment
  }
}
