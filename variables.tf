variable "environment" {
  type        = string
  description = "Environment name"
  default     = "changeme"
}

variable "name" {
  type        = string
  description = "Project name"
  default     = "changeme"
}

variable "node_type" {
  type        = string
  description = "Note type for cache description"
  default     = "cache.t3.medium"
}

variable "num_cache_nodes" {
  type        = number
  description = "Cluster node size"
  default     = 1
}

variable "port" {
  type        = number
  description = "Redis port"
  default     = 6379 
}

variable "subnet_ids" {
  type        = list(string)
  description = "Subnet identifier list"
  default     = []
}

variable "security_group_ids" {
  type        = list(string)
  description = "Security Groups identifier list"
  default     = []
}

variable "snapshot_retention_days" {
  type        = number
  description = "Snapshot retention amount of days"
  default     = 5
}

variable "snapshot_window" {
  type        = string
  description = "Snapshot window"
  default     = "01:00-02:00"
}
