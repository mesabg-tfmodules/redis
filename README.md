# Redis Module

This module is capable to create a Redis cluster on AWS ElasticCache

Module Input Variables
----------------------

- `environment` - environment name.
- `name` - redis cluster name name.
- `node_type` - redis node type (default cache.t3.medium).
- `num_cache_nodes` - redis node nodes counter (default 1).
- `port` - redis port (default 6379).
- `subnet_ids` - subnet identifiers.
- `security_group_ids` - security group identifiers.
- `snapshot_retention_days` - amount of days to retain the snapshots (default 5).
- `snapshot_window` - which time of day to take the snapshots (default 01:00-02:00).

Usage
-----

```hcl
module "redis" {
  source                  = "git::https://gitlab.com/mesabg-tfmodules/redis.git"

  environment             = "environment"

  name                    = "name"
  node_type               = "cache.t3.medium"
  num_cache_nodes         = 1
  port                    = 6379

  subnet_ids              = ["subnet-xxxx", "subnet-wwww"]
  security_group_ids      = ["sg-xxxx", "sg-aaaa"]

  snapshot_retention_days = 5
  snapshot_window         = "01:00-02:00"
}
```

```hcl
module "redis" {
  source              = "git::https://gitlab.com/mesabg-tfmodules/redis.git"

  environment         = "environment"

  name                = "name"

  subnet_ids          = ["subnet-xxxx", "subnet-wwww"]
  security_group_ids  = ["sg-xxxx", "sg-aaaa"]
}
```

Outputs
=======

 - `cache_nodes` - List of node objects including id, address, port and availability_zone.

Authors
=======
##### Moisés Berenguer <moises.berenguer@gmail.com>
