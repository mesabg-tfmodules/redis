output "cache_nodes" {
  value       = aws_elasticache_cluster.redis.cache_nodes
  description = "List of node objects including id, address, port and availability_zone."
}
